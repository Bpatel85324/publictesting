FROM openjdk:8u111-jdk-alpine
LABEL "org.opencontainers.image.vendor"="1800Flowers.com"

COPY /target/template-example-*.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
